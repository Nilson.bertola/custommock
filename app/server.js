require('dotenv').config();

const bodyParser = require('body-parser');
const config = require('../config');
const cors = require('cors');
const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const routes = require('./routes');
const middleware = require('./middleware');

const {PORT} = config;
const port = PORT || 8080;

const app = express();

app.get('/health', (_, response) =>
  response.send({msg: 'Everything is good!'}),
);

app.use(
    morgan('combined'),
    helmet(),
    cors(),
    bodyParser.json(),
    bodyParser.urlencoded({extended: true}),
    middleware.appSecret,
);

app.use(routes);

app.listen(port, () => console.info(`Server is live on port: ${port}`));
