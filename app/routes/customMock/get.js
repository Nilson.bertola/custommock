const axios = require('axios');
const cheerio = require('cheerio');

const app = (module.exports = require('express')());

app.get('/', async (request, response) => {
  const {path} = request.query;
  const {data} = await axios.get('http://dontpad.com/' + path);
  const $ = cheerio.load(data);
  let content = null;
  if ($('body').find('#text')[0].children[0]) {
    content = $('body').find('#text')[0].children[0].data;
  }
  response.json({
    status: 200,
    data: JSON.parse(content),
  });
});
