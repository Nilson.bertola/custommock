const app = (module.exports = require('express')());

app.use('/system', require('./system'));
app.use('/custom', require('./customMock'));
