const {APP_SECRET, forbiddenStatusCode} = require('../../config');
const {checkValidVariable} = require('../helper');
module.exports = (request, response, next) => {
  if (checkValidVariable(APP_SECRET)) {
    if (request.headers.app_secret === APP_SECRET) {
      next();
    } else {
      response.status(forbiddenStatusCode).send('Forbidden!');
    }
  } else {
    next();
  }
};
